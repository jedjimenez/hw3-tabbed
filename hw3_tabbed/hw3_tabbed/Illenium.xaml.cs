﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace hw3_tabbed
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Illenium : ContentPage
	{
        int counter = 0;

        public Illenium ()
		{
			InitializeComponent ();
		}

        //when the button is clicked, corresponding events occur such as changing text color and image
        async void OnClicked(object sender, EventArgs e)
        {
            counter++;
            switch (counter)
            {
                case 1:
                    b.Text = "Change Text Color";                            
                    img.Source = "illenium_logo.png";
                    break;
                case 2:
                    t.TextColor = Color.OrangeRed;
                    t1.TextColor = Color.OrangeRed;
                    t2.TextColor = Color.OrangeRed;
                    t3.TextColor = Color.OrangeRed;
                    b.TextColor = Color.OrangeRed;
                    b.Text = "Back to Original";
                    break;
                case 3:
                    t.TextColor = Color.Black;
                    t1.TextColor = Color.Black;
                    t2.TextColor = Color.Black;
                    t3.TextColor = Color.Black;
                    b.TextColor = Color.Black;
                    img.Source = "illenium.png";
                    b.Text = "Back to Artists";
                    break;
                case 4:
                    await Navigation.PopAsync();
                    break;

            }
        }

        //displays an alert when leaving the page
        async protected override void OnDisappearing()
        {
            await DisplayAlert("Goodbye", "Now leaving Illenium", "OK");
        }
    }
}