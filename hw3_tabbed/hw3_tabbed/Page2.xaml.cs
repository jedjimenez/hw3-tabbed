﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace hw3_tabbed
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Page2 : ContentPage
	{
		public Page2 ()
		{
			InitializeComponent ();
		}

        protected override void OnDisappearing()
        {
            img.Source = "illenium.png";
            img2.Source = "gryffin.jpg";
            img3.Source = "sanholo.jpg";
            img4.Source = "slander.jpg";

        }

        //when top left image is clicked, it goes to a different page about that artist, and the image changes to their icon
        async void IClicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Illenium());
            img.Source = "illenium_logo.png";
        }

        //when top right image is clicked, it goes to a different page about that artist, and the image changes to their icon
        async void GClicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Gryffin());
            img2.Source = "gryffin_logo.png";
        }

        //when bottom left image is clicked, it goes to a different page about that artist, and the image changes to their icon
        async void SHClicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new SanHolo());
            img3.Source = "sanholo_logo.png";
        }

        //when bottom right image is clicked, it goes to a different page about that artist, and the image changes to their icon
        async void SClicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Slander());
            img4.Source = "slander_logo.png";
        }

    }
}