﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace hw3_tabbed
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Gryffin : ContentPage
	{
        int counter = 0;

		public Gryffin ()
		{
			InitializeComponent ();
		}


        //when the button is clicked, corresponding events occur such as changing text color and image
        async void OnClicked(object sender, EventArgs e)
        {
            counter++;
            switch (counter)
            {
                case 1:
                    t.TextColor = Color.Red;
                    t1.TextColor = Color.Red;
                    t2.TextColor = Color.Red;
                    t3.TextColor = Color.Red;
                    b.TextColor = Color.Red;
                    b.Text = "Back to Original";
                    img.Source = "gryffin_logo.png";
                    BackgroundColor = Color.SlateGray;
                    break;
                case 2:
                    t.TextColor = Color.Black;
                    t1.TextColor = Color.Black;
                    t2.TextColor = Color.Black;
                    t3.TextColor = Color.Black;
                    b.TextColor = Color.Black;
                    b.Text = "Back to Artists";
                    img.Source = "gryffin.jpg";
                    BackgroundColor = Color.Moccasin;
                    break;
                case 3:
                    await Navigation.PopAsync();
                    break;
                

            }
        }

        //displays an alert when leaving the page
        async protected override void OnDisappearing()
        {
            await DisplayAlert("Goodbye", "Now leaving Gryffin", "OK");
        }
    }
}