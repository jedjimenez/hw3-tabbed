﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace hw3_tabbed
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page1 : ContentPage
    {
        public Page1()
        {
            InitializeComponent();
        }

        //when you move to a different page and come back to the page, it changes the picture, a button appears and disappears 
        protected override void OnDisappearing()
        {
            img.Source = "edc.png";

            var button = new Button
            {
                Text = "Click for original image",
                VerticalOptions = LayoutOptions.CenterAndExpand,
                HorizontalOptions = LayoutOptions.Center,
                BackgroundColor = Color.DarkSlateGray,
                TextColor = Color.White
            };
            g.Children.Add(button, 1, 6); //column and row
            button.Clicked += async (sender, args) => img.Source = "main.png"; //when button is clicked, it changes the picture back to original
            button.Clicked += async (sender, args) => button.IsVisible = false; //when button is clicked, it hides the button
        }

    }
}