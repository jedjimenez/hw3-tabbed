﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace hw3_tabbed
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Slander : ContentPage
	{
		public Slander ()
		{
			InitializeComponent ();
		}

        //displays an alert when leaving the page
        async protected override void OnDisappearing()
        {
            await DisplayAlert("Goodbye", "Now leaving Slander", "OK");
        }

        //when switch is on it changes the text, text color, and background color. when switch is off then it stays default
        void OnToggled(object sender, ToggledEventArgs e)
        {
            if (e.Value == true)
            {
                t.TextColor = Color.Green;
                t1.TextColor = Color.Green;
                t2.TextColor = Color.Green;
                t3.TextColor = Color.Green;
                t4.Text = "Surprise!";
                t4.TextColor = Color.Green;
                BackgroundColor = Color.MidnightBlue;
            }
            else
            {
                t.TextColor = Color.Black;
                t1.TextColor = Color.Black;
                t2.TextColor = Color.Black;
                t3.TextColor = Color.Black;
                t4.Text = "Use the switch for surprise!";
                t4.TextColor = Color.Black;
                BackgroundColor = Color.Moccasin;
            }
      
        }
    }
}