﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace hw3_tabbed
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SanHolo : ContentPage
	{
		public SanHolo ()
		{
			InitializeComponent ();
		}

        //slider that rotates the text and image when used
        void OnSliderValueChanged(object sender, ValueChangedEventArgs args)
        {
            double value = args.NewValue;
            t.Rotation = value;
            t1.Rotation = value;
            t2.Rotation = value;
            t3.Rotation = value;
            img.Rotation = value;
        }

        //displays an alert when leaving the page
        async protected override void OnDisappearing()
        {
            await DisplayAlert("Goodbye", "Now leaving San Holo", "OK");
        }
    }
}