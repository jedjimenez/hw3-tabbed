﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace hw3_tabbed
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Page3 : ContentPage
	{
        public Page3()
        {
            InitializeComponent();
        }

        //when top image button is clicked, it redirects to a url, it changes text and color, and background color
        async void EDC(object sender, EventArgs e)
        {
            t.Text = "You are looking at EDC!";
            t.TextColor = Color.Yellow;
            BackgroundColor = Color.Magenta;
            Device.OpenUri(new Uri("https://lasvegas.electricdaisycarnival.com/"));
        }

        //when bottom image button is clicked, it redirects to a url, it changes text and color, and background color
        async void Tomorrowland(object sender, EventArgs e)
        {
            BackgroundColor = Color.DodgerBlue;
            t.TextColor = Color.SeaShell;
            t.Text = "You are looking at Tomorrowland!";
            Device.OpenUri(new Uri("https://www.tomorrowland.com/global/"));
        }

        //when middle image button is clicked, it redirects to a url, it changes text and color, and background color
        async void UMF(object sender, EventArgs e)
        {
            BackgroundColor = Color.MidnightBlue;
            t.Text = "You are looking at Ultra Music Festival";
            t.TextColor = Color.Gold;
            Device.OpenUri(new Uri("https://ultramusicfestival.com/"));
        }
    }
}